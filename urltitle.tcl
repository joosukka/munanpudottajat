#!
##############################################################################################
# url title script. By; madpinger: admin at madpinger dot com.
# Oct 28 2010, initial release.
# Oct 30 2010
# Removed ugly comments at header, changed and added comments. Read the README !

# Variables
set cliDebug 1 ;# display extra debug info. Intended for the cli wrapper, tho will work in console/dcc
set urlLimit 2 ;# loop limit, counts from 0
set urlLogo "\002\[URL\]\002" ;# prefix to the line sent to the channel
set urlUA "Mozilla/5.0 (X11; Debian; Linux x86_64; rv:16.0) Gecko/20100101 Firefox/16.0" ;# User agent to identify to server as.
set urlHeaders [list "Accept-Language" "fi-fi,fi;q=0.8,en-gb;q=0.5,en;q=0.3" "Accept-Encoding" " " "Accept-Charset" "UTF-8,*"]; # headers to send with get.
set urlTimeOut 10000 ;# timeout in miliseconds.
set urlIgnore [list {^http://gyazo.com/}] ;# List of addresses to ignore, give as a regexp pattern
set ::SAextra ""

# Chan flag
# You will need this flag on any channel you intend to use it on (ie. .chanset #channel +urltitle)
setudef flag urltitle

# Binds
bind PUBM - * pub_url	

# Prevent flooding
array set ::urltitleflood {}

# Load packages
package require http
package require tls
::http::register https 443 [list ::tls::socket -tls1 1]

###############################################################################
# The code,  If you feel comfortable modifying it, have fun.
###############################################################################

# This allows us to check for servers that support the +c chanmode and if needed
# to strip all the codes in the string before sending it to the server.
proc putmsg {dest text {queue "help"} {extra "-normal"}} {
  if {$queue != "serv"} {
    if {[validchan $dest] && [botonchan $dest] && [string first c [lindex [split [getchanmode $dest]] 0]] != -1} {
        set text [stripcodes bcruag $text]
    }
  }
  put$queue "PRIVMSG $dest :$text" $extra
}

# Return filesize in human readable format
proc humanify {size {digits 2}} {
    #foreach prefix {B kiB MiB GiB TiB PiB} {}
    if {$size == ""} { set size 0 }
    foreach prefix {B kB MB GB TB PB} {
        set new [expr {$size/1024.0}]
        if {$new < 1} { break }
       	set size $new
    }
    return "[format %.*f $digits $size] $prefix"
}

# Convert HTML Entities to unicode, updated with speechless's new version. See readme.
proc convertHE {text {char "utf-8"}} {
   # code below is neccessary to prevent numerous html markups
   # from appearing in the output (ie, &quot;, ᘧ, etc)
   # stolen (borrowed is a better term) from tcllib's htmlparse ;)
   # works unpatched utf-8 or not, unlike htmlparse::mapEscapes
   # which will only work properly patched....
   if {$::cliDebug} { putlog "plugin:convertHE:system encoding:[encoding system]" }
   if {$::cliDebug } { putlog "plugin:convertHE:text: $text :char: $char" }
   set escapes {
      &nbsp; \xa0 &iexcl; \xa1 &cent; \xa2 &pound; \xa3 &curren; \xa4
      &yen; \xa5 &brvbar; \xa6 &sect; \xa7 &uml; \xa8 &copy; \xa9
      &ordf; \xaa &laquo; \xab &not; \xac &shy; \xad &reg; \xae
      &macr; \xaf &deg; \xb0 &plusmn; \xb1 &sup2; \xb2 &sup3; \xb3
      &acute; \xb4 &micro; \xb5 &para; \xb6 &middot; \xb7 &cedil; \xb8
      &sup1; \xb9 &ordm; \xba &raquo; \xbb &frac14; \xbc &frac12; \xbd
      &frac34; \xbe &iquest; \xbf &Agrave; \xc0 &Aacute; \xc1 &Acirc; \xc2
      &Atilde; \xc3 &Auml; \xc4 &Aring; \xc5 &AElig; \xc6 &Ccedil; \xc7
      &Egrave; \xc8 &Eacute; \xc9 &Ecirc; \xca &Euml; \xcb &Igrave; \xcc
      &Iacute; \xcd &Icirc; \xce &Iuml; \xcf &ETH; \xd0 &Ntilde; \xd1
      &Ograve; \xd2 &Oacute; \xd3 &Ocirc; \xd4 &Otilde; \xd5 &Ouml; \xd6
      &times; \xd7 &Oslash; \xd8 &Ugrave; \xd9 &Uacute; \xda &Ucirc; \xdb
      &Uuml; \xdc &Yacute; \xdd &THORN; \xde &szlig; \xdf &agrave; \xe0
      &aacute; \xe1 &acirc; \xe2 &atilde; \xe3 &auml; \xe4 &aring; \xe5
      &aelig; \xe6 &ccedil; \xe7 &egrave; \xe8 &eacute; \xe9 &ecirc; \xea
      &euml; \xeb &igrave; \xec &iacute; \xed &icirc; \xee &iuml; \xef
      &eth; \xf0 &ntilde; \xf1 &ograve; \xf2 &oacute; \xf3 &ocirc; \xf4
      &otilde; \xf5 &ouml; \xf6 &divide; \xf7 &oslash; \xf8 &ugrave; \xf9
      &uacute; \xfa &ucirc; \xfb &uuml; \xfc &yacute; \xfd &thorn; \xfe
      &yuml; \xff &fnof; \u192 &Alpha; \u391 &Beta; \u392 &Gamma; \u393 &Delta; \u394
      &Epsilon; \u395 &Zeta; \u396 &Eta; \u397 &Theta; \u398 &Iota; \u399
      &Kappa; \u39A &Lambda; \u39B &Mu; \u39C &Nu; \u39D &Xi; \u39E
      &Omicron; \u39F &Pi; \u3A0 &Rho; \u3A1 &Sigma; \u3A3 &Tau; \u3A4
      &Upsilon; \u3A5 &Phi; \u3A6 &Chi; \u3A7 &Psi; \u3A8 &Omega; \u3A9
      &alpha; \u3B1 &beta; \u3B2 &gamma; \u3B3 &delta; \u3B4 &epsilon; \u3B5
      &zeta; \u3B6 &eta; \u3B7 &theta; \u3B8 &iota; \u3B9 &kappa; \u3BA
      &lambda; \u3BB &mu; \u3BC &nu; \u3BD &xi; \u3BE &omicron; \u3BF
      &pi; \u3C0 &rho; \u3C1 &sigmaf; \u3C2 &sigma; \u3C3 &tau; \u3C4
      &upsilon; \u3C5 &phi; \u3C6 &chi; \u3C7 &psi; \u3C8 &omega; \u3C9
      &thetasym; \u3D1 &upsih; \u3D2 &piv; \u3D6 &bull; \u2022
      &hellip; \u2026 &prime; \u2032 &Prime; \u2033 &oline; \u203E
      &frasl; \u2044 &weierp; \u2118 &image; \u2111 &real; \u211C
      &trade; \u2122 &alefsym; \u2135 &larr; \u2190 &uarr; \u2191
      &rarr; \u2192 &darr; \u2193 &harr; \u2194 &crarr; \u21B5
      &lArr; \u21D0 &uArr; \u21D1 &rArr; \u21D2 &dArr; \u21D3 &hArr; \u21D4
      &forall; \u2200 &part; \u2202 &exist; \u2203 &empty; \u2205
      &nabla; \u2207 &isin; \u2208 &notin; \u2209 &ni; \u220B &prod; \u220F
      &sum; \u2211 &minus; \u2212 &lowast; \u2217 &radic; \u221A
      &prop; \u221D &infin; \u221E &ang; \u2220 &and; \u2227 &or; \u2228
      &cap; \u2229 &cup; \u222A &int; \u222B &there4; \u2234 &sim; \u223C
      &cong; \u2245 &asymp; \u2248 &ne; \u2260 &equiv; \u2261 &le; \u2264
      &ge; \u2265 &sub; \u2282 &sup; \u2283 &nsub; \u2284 &sube; \u2286
      &supe; \u2287 &oplus; \u2295 &otimes; \u2297 &perp; \u22A5
      &sdot; \u22C5 &lceil; \u2308 &rceil; \u2309 &lfloor; \u230A
      &rfloor; \u230B &lang; \u2329 &rang; \u232A &loz; \u25CA
      &spades; \u2660 &clubs; \u2663 &hearts; \u2665 &diams; \u2666
      &quot; \x22 &amp; \x26 &lt; \x3C &gt; \x3E O&Elig; \u152 &oelig; \u153
      &Scaron; \u160 &scaron; \u161 &Yuml; \u178 &circ; \u2C6
      &tilde; \u2DC &ensp; \u2002 &emsp; \u2003 &thinsp; \u2009
      &zwnj; \u200C &zwj; \u200D &lrm; \u200E &rlm; \u200F &ndash; \u2013
      &mdash; \u2014 &lsquo; \u2018 &rsquo; \u2019 &sbquo; \u201A
      &ldquo; \u201C &rdquo; \u201D &bdquo; \u201E &dagger; \u2020
      &Dagger; \u2021 &permil; \u2030 &lsaquo; \u2039 &rsaquo; \u203A
      &euro; \u20AC &apos; \u0027 &lrm; "" &rlm; ""
   };
   #if {![string equal $char [encoding system]]} { set text [encoding convertfrom $char $text]; if {$::cliDebug} { putlog "plugin:convertHE:convertfrom $char:$text" } }

   set text [encoding convertfrom $char $text]; if {$::cliDebug} { putlog "plugin:convertHE:convertfrom $char:$text" }
   #set text [string map [list "\]" "\\\]" "\[" "\\\[" "\$" "\\\$" "\"" "\\\"" "\\" "\\\\"] [string map -nocase $escapes $text]]
   set text [string map [list "\]" "\\\]" "\[" "\\\[" "\$" "\\\$" "\"" "\\\"" "\\" "\\\\"] [string map $escapes $text]]
   regsub -all -- {&#([[:digit:]]{1,5});} $text {[format %c [string trimleft "\1" "0"]]} text
   regsub -all -- {&#x([[:xdigit:]]{1,4});} $text {[format %c [scan "\1" %x]]} text
   catch { set text "[subst "$text"]" }
   #My bot is utf-8 and system is utf-8, so I comment this out on mine or it breaks the string.
   #if {![string equal $char [encoding system]]} { set text [encoding convertto $char $text]; if {$::cliDebug} { putlog "plugin:convertHE:convertto [encoding system]:$text" } }
   return "$text"
}

#map common charset meta-data strings used in sites to a string that is compatible with tcl's encoding names
proc convertCharSet {text} {
  set escapes {
    iso- iso windows- cp shift_jis shiftjis
    utf8 utf-8 euc_jp euc-jp
  }
  return [string map $escapes [string tolower $text]]
}

#The main proc
proc pub_url {nick host hand channel text {redirect 0}} {
    #check the channel flag before starting
    if {![channel get $channel urltitle]} {
      return 1
    }

	#set count before entering loop
    set urlcount 0
    foreach word [split $text] {
	set isredirect 0
	set haserror 0
	set extra ""
	set urlUA $::urlUA
	if {$redirect == 0} { set ::SAextra "" }

	#hax for facebook pictures
	#if {[regexp {^https?://.*?\.(?:fbcdn\.net|akamaihd\.net)/.*?\d*?_(\d*?)_(\d*?)_.*?\.jpg} $word -> fbid id]} {
	#	set word "http://www.facebook.com/profile.php?id=$id"
	#	set ::SAextra " - $word"
	#}

	#hax for irc-galleria pictures
	if {[regexp {^https?://.*?\.irc-galleria\.net/.*?/(\d{2})/(\d{2})/(\d{2})/(\d{2})/(\d*?)\.jpg} $word -> id1 id2 id3 id4 image]} {
		#lappend urlHeaders "Cookie" "uid=${id1}${id2}${id3}${id4}"
		#set word "http://irc-galleria.net/user/uid?uid=${id1}${id2}${id3}${id4}&image_id=${image}"
		set word "http://irc-galleria.net/user/uid?uid=${id1}${id2}${id3}${id4}"
		set ::SAextra " - $word"
	}

	#hax for youtube
#	if {[regexp {^https?://(?:youtu\.be/([\w-]*)/?)|(?:(?:.*?\.)?youtube\.com/watch\?.*?v=([\w-]*))} $word -> id1 id2]} {
#		putlog "youtube \"$id1\" \"$id2\""
#		set word "http://gdata.youtube.com/feeds/api/videos/${id1}${id2}?v=2"
#		set ::SAextra " - YouTube"
#	}
	
	#hax for spotify
	if {[regexp {^spotify:(.*?):(.*?)$} $word -> id1 id2]} {
		putlog "spotify \"$id1\" \"$id2\""
		set urlUA ""
		set word "http://open.spotify.com/${id1}/${id2}"
		set ::SAextra " - $word"
	}
	
	#hax for northpole
	set urlHeaders $::urlHeaders
	if {[regexp {^https?://northpole\.fi/.*?(?:/|$)} $word]} {
		lappend urlHeaders "Cookie" "uid=12bbe7960e8489e13a6358c9e8c93f44a9d52361"
	}

	#check if the current word in list matches a valid url
        if {[regexp {^(f|ht)tp(s|)://} $word] && ![regexp {://([^/:]*:([^/]*@|\d+(/|$))|.*/\.)} $word]} {

		if {$redirect == 0} {
			if {![info exists ::urltitleflood($channel)]} {
				set ::urltitleflood($channel) 0
			}

			if {([expr [unixtime] - 5] > $::urltitleflood($channel))} {
				set ::urltitleflood($channel) [unixtime]
			} else {
				return 1
			}
		}
		
	    # if you want to allow url:ports you can change the second regexp or just remove it, tho this is done with intent as well as avoiding email addresses, etc
	    incr urlcount ;# incriment the loop count, incr is pref to expr.
	    #if the variable word exist and has a length greater than 0, fetch the page
            if {[info exists word] && [string length $word]} {
				#check the ignore list
		#if {[info exist $::urlIgnore]} {
		foreach ignore $::urlIgnore {
			if {[regexp $ignore $word]} {
				return 1
			}
		}
		#}
		
	    set http [::http::config -useragent $urlUA -urlencoding "utf-8" \
		      -accept "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8"]
                catch {set tok [::http::geturl $word -timeout $::urlTimeOut -headers $urlHeaders -myaddr "188.165.140.5"]} errmsg
		set newdata ""
		set charenc 0
		set title ""

		set status [::http::ncode $tok]
		putlog "URL: $word Status: $status"
		# Lazy hack to ignore spotifys redirect
		#if {[regexp {open.spotify.com/track/} $word] && ($status==302)} { set status 200 }
		switch $status {
		    "301" -
		    "303" -
		    "302" {
			if {$redirect != 1} {
			# Handle URL redirects
			    upvar #0 $tok state
			    foreach {name value} $state(meta) {
			        if {[regexp -nocase ^location$ $name]} {
					putlog "redirect $value"
			            pub_url $nick $host $hand $channel $value 1;#call ourself again with the extra value !
				    set isredirect 1 ;# make sure that since we have to finish the loop to get all url's that we don't spam 301 redirects to the channel
			        }
			    }
			}
			set haserror 1
			::http::cleanup $tok
		    }
		    "404" {
			set haserror 1
			::http::cleanup $tok
		    }
		    "418" {
		      # Good ole humor.
			putmsg $channel "$nick, Did you know that $word is a short stout teapot ?"
			set haserror 1
			::http::cleanup $tok
		    }
		    "200" {
				# 200 ok, recieved data from our get.
				if {[info exists $errmsg] && [string match -nocase "*couldn't open socket*" $errmsg]} {
					putloglev d $channel "plugin:pub_url:$errmsg"
					set haserror 1
				}
				if { [::http::status $tok] == "timeout" } {
					putloglev d $channel "plugin:pub_url:timeout:$url"
					set haserror 1
				}
				set data [::http::data $tok]
				set meta [::http::meta $tok]
				::http::cleanup $tok

				array set meta_array $meta
				set contenttype [lindex [array get meta_array {[Cc]ontent-[Tt]ype}] 1]
				set contentlength [lindex [array get meta_array {[Cc]ontent-[Ll]ength}] 1]
				putlog "Content-Type: $contenttype"
				#putmsg $channel "URL DEBUG: Type: $contenttype Size: [humanify $contentlength]"
				regexp -nocase {charset=(.*?)(?:;|$)} $contenttype  -> charenc
				foreach line [split $data \n] {
					#putdcc [hand2idx "joose"] $line 
				    if {[regexp -nocase {<meta.*charset.(.*?)".*>} $line -> charset]} {
						set charenc $charset
				    }
					append newdata " [string trim $line]"
				}
				if {![regexp -nocase {<title.*?>(.*?)</title>} $newdata match title]} {
					set haserror 1
				}
		    }
		    default { set haserror 1 }
		}
            }
            # Output section
            if {[string length $title] && $isredirect != 1 && $haserror != 1} {
                    # Lazy hack to ignore weird characters in Youtube titles
                    #if {[regexp {(http\:\/\/|\.)youtube.com} $word]} {set title [string map {"&#x202a;" "" "&#x202c;" "" "&rlm;" ""} $title]}
				if {[string length $charenc] > 1} { set charenc [convertCharSet $charenc] }
				if {$charenc != 0 && [string length $charenc] > 1 && $charenc != "unicode" && [lsearch -exact [encoding names] $charenc]} {
					if {$::cliDebug } { putlog "plugin:pub_url:charenc:$charenc:valid" }
					set title [convertHE [string trim $title] $charenc]
				} else {
					if {$::cliDebug } { putlog "plugin:pub_url:charenc:$charenc:invalid" }
					set title [convertHE [string trim $title]]
				}
				putmsg $channel "$::urlLogo ${title}${::SAextra}"
				set ::SAextra ""
            }
	}
	if {$urlcount > $::urlLimit} {
	    break ;# reached limit
	}
	# Repeat
    }
}

putlog "Loaded urltitle.tcl"
#FIN
