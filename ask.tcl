# Simple script that allows you to ask questions from bot. Say !ask <your question>. By: Matti Aronen
set sanat_pos {
"Kyllä."
"Ehdottomasti kyllä."
"Totta helvetissä!"
"Huhut kertovat kyllä."
"Tietenkin, lopeta typerien kysymysten esittäminen."
"Toki."
}
#"\001ACTION nyökkää.\001"

set sanat_neg {
"Ei."
"En löisi vetoa siitä."
"Ehdottomasti ei."
"Ei välttämättä."
"Ei missään tapauksessa."
"Nein, nein, nein!"
"Hulluko olet, ei tietenkään!"
}
#"\001ACTION pudistaa päätään.\001"

set sanat_neut {
"En tiedä."
"Heitä kolikkoa."
"Hmm, kysymyksesi on niin kryptinen etten ole edes varma mitä kysyt."
"Kysy kaikkitietävältä Hipsulta."
"Kojida sopii kaikille."
}

set sanat_vai {
"."
"tietenkin!"
}

set vanhat_sanat {
"Ehdottomasti ehkä."
"Passaan."
"En tiedä kaikkea."
"JIioauOADjhjapfsajfisjIJASFipjafjapfioasf"
"Kyllä ehkä, tai sitten ei."
"Saattaa olla."
"Kysy joltakulta muulta, olen liian väsynyt."
"Sinä et tarvitse sitä tietoa."
"Anna minulle rahasi ja kysy uudestaan."
"En tiedä :3"
"Jaa: 78 Ei: 93 Tyhjiä: 4 Poissa: 15"
"Kysy Amerikan Yhdysvaltojen presidentiltä."
"Saattaa olla."
"Ammuu."
"..."
"En vastaa tuohon."
"Suattaapi olla tai ei suata!"
"Mistä minä tietäisin?"
"En kommentoi."
"Vastaus kysymykseesi löytyy meditoimalla."
"Näin on närhenmunat!"
"spurdo spärde sprölölölölööööö"
}

setudef flag ask

bind pub - !ask tell_that
bind pub - !kysy tell_that
proc tell_that {nick uhost hand chan text} {
	if {[channel get $chan "ask"]} {
		global sanat
		if {$text == ""} {
			putchan $chan "$nick: Kristallipalloni on juuri nyt huollossa."
		} elseif {[regexp -nocase {(\w(?:\w| )+) (?:vai|or) ((?:\w| )+)} $text -> asia1 asia2]} {
			if {[rand 2] == 1} {
				set out $asia1
			} else {
				set out $asia2
			}

			putchan $chan "$nick: $out tietenkin!"
		} elseif {[regexp -nocase {(?:^|\ )joose(?:\ |$)} $text]} {
			putchan $chan "$nick: Kysy Jooselta."
		} else {
			set rand [rand 5]
			if {$rand<=1} {
				set out [lindex $::sanat_pos [rand [llength $::sanat_pos]]]
			} elseif {$rand==2} {
				set out [lindex $::sanat_neut [rand [llength $::sanat_neut]]]
			} elseif {$rand>=3} {
				set out [lindex $::sanat_neg [rand [llength $::sanat_neg]]]
			}

			putchan $chan "$nick: $out"
		}
	}
}

putlog "ask.tcl loaded"
