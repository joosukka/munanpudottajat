# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#  Sää v. 2.0                                                                             #
#  Näyttää säätietoja eri paikkakunnilta                                                  #
#                                                                                         #
#  Kanavat joilla botin sallitaan säätietoja näyttävän asetetaan partylinessä komennolla  #
#   .chanset #kanava [+|-]saa                                                             #
#  Voi myös asettaa scriptin toimimaan kaikilla kanavilla Asetukset-kohdassa              #
#                                                                                         #
#  Käyttö: !sää <paikkakunta>                                                             #
#                                                                                         #
#  Hakee sään osoitteesta foreca.fi                                                       #
#                                                                                         #
#  Code by Joose                                                                          #
#  [joose ät joose piste biz]                                                             #
#  http://code.joose.biz/eggdrop/                                                         #
#                                                                                         #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

namespace eval saa_foreca {

	# ---------------------------------------------------------------------------------------------------------- #
	# Asetukset

	# Komento (tai komennot) joilla skripti aktivoituu (oletus "!sää !saa")
	set bind "%saa2"
	
	# Määrittele mitä botti sanoo kanavalle
	# %station% = havaintoasema ("Espoo Sepänkylä")
	# %weather% = säätila ("Tuorein säähavainto: 14.1.2010 6:00 Suomen aikaa Lämpötila -5,9 °C; kosteus 99 %; kastepiste -6,1 °C (5:50); länsituulta 2 m/s; puuska 4 m/s (5:50); paine 1024,5 hPa.")
	# %time% = auringon ajat + päivän pituus (jos ei saatavilla, jää tämä kohta on tyhjäksi) ("Aurinko nousee 9:12 ja laskee 15:49. Päivän pituus on 6 h 37 min.")
	# (oletus "Havaintoasema %station% %weather% %time%")
	set output "Havaintoasema: %station% %weather% %time%"
	
	# Määrittelee vakio-sijainnin jota käytetään jos komentoa käytetään yksinään
	# Jätä tyhjäksi jos haluat että näytetään vain virhe-viesti
	set default_location "Helsinki"
	
	# Määrittelee toimiiko botti oletuksena kaikilla kanavilla
	# 0 = vain partylinessä määritellyt, 1 = kaikki kanavat
	set allchan 1
	
	# Jos edellinen kohta on 1, voit tässä määritellä kanavat joilla botti EI vastaa (erottele välilyönnillä)
	set allchan_blacklist ""
	
	# Sekunteina kuinka usein scriptiä voi käyttää
	set rest_time 5
	
	# Määrittelee onko lepoaika (kts. edellinen kohta) kanavakohtainen vai globaali
	# 1 = globaali, 2 = kanavakohtainen
	set restmode 2
	
	# Määrittele miten käyttäjälle ilmoitetaan että lepoaika on voimassa
	# 0 = ei ilmoitusta, 1 = NOTICE, 2 = PRIVMSG
	set rest_msg_method 1
	
	# Määrittele viesti mikä käyttäjälle ilmoitetaan jos lepoaika on voimassa
	# %time% = jäljellä oleva aika, %channel% = kanava jossa komentoa käytettiin
	set rest_msg "%channel%: Ethän floodaa komentoja (Odota %time% sekuntia)"
	
	# Määrittele mitä flagia käyeteään debug-tulostukselle (default d, eli partylinessä: .console +d )
	set debugflag d
	
	# Älä muuta tästä eteenpäin ellet tiedä mitä teet
	# ---------------------------------------------------------------------------------------------------------- #

	
	set scriptversion "Sää (v. 3.0 Foreca) by Joose"
	set protection ""
	set allchan_blacklist [string tolower $allchan_blacklist]
	
	foreach bind [split $saa_foreca::bind " "] {
		bind pub - $bind saa_foreca::pubWeather
	}
	
	if { $saa_foreca::allchan == 0 } { setudef flag saa }
	if { $saa_foreca::restmode == 2 } { array set rest {}}

	proc pubWeather {nick host hand chan text} {
		set resting ""
		if { (($saa_foreca::allchan == 0) && ([channel get $chan saa])) || (($saa_foreca::allchan == 1) && ([lsearch [split $saa_foreca::allchan_blacklist] [string tolower $chan]] == -1)) }  {
			if {($saa_foreca::restmode == 1)} {
				if {([expr [unixtime] - $saa_foreca::rest_time] > $saa_foreca::protection)} {
					set saa_foreca::protection [unixtime]
					set resting 0
				} else {
					set resting [expr $saa_foreca::rest_time - [unixtime] + $saa_foreca::protection]
				}
			} elseif {($saa_foreca::restmode == 2)} { 
				if {([expr [unixtime] - $saa_foreca::rest_time] > [lindex [array get saa_foreca::rest $chan] 1])} {
					array set saa_foreca::rest [list $chan [unixtime]]
					set resting 0
				} else {
					set resting [expr [lindex [array get saa_foreca::rest $chan] 1] - [unixtime] + $saa_foreca::rest_time]
				}
			} else { return }
			
			if { $resting == 0 } {
				set haku [saa_foreca::getWeather $text]
				putserv "PRIVMSG $chan :$haku"
				return
			} elseif { $resting > 0 } {
				putloglev $saa_foreca::debugflag * "Debug: Sää resting on channel $chan for $resting seconds"
				set rest_msg [string map [list "%time%" $resting "%channel%" $chan] $saa_foreca::rest_msg]
				switch -- $saa_foreca::rest_msg_method {
					0 {}
					1 {puthelp "NOTICE $nick :$rest_msg"}
					2 {puthelp "PRIVMSG $nick :$rest_msg"}
				}
				return
			} else { return }	
		} else { return }
	}

	proc getWeather { text } {
		set id ""
		set loc ""
		set div ""
		set continent ""
		set country ""
		set state ""
		set place ""
		set station ""
		set output ""
		
		if {($text=="") && ($saa_foreca::default_location=="")} {
			return "Et antanut paikannimeä!"
		} elseif {($text=="") && !($saa_foreca::default_location=="")} {
			set text $saa_foreca::default_location
		}
		
		set host "foreca.fi"
		
		set url "/complete.php"
		set post "q=[url_encode $text]"
		set data [saa_foreca::makequery $host $url $post]
		regexp {<li id="(.*?)">.*?</li>} $data -> id
		
		if {$id == ""} { return "Paikannimellä \"$text\" ei löytynyt säätietoja" }
		
		set url "/search"
		set post "loc_id=[url_encode $id]&q=[url_encode $text]"
		set data [saa_foreca::makequery $host $url $post]
		regexp -line {^Location: (.*)$} $data -> loc

		set data [saa_foreca::makequery $host $loc]
		putlog "http://$host$loc"
		#regexp {<div class="wrap-area entry-content">(.*)</div>} $data -> data

		#putdcc [hand2idx "joose"] "---------------------------------------"
		#foreach line [regexp -inline -all {.{0,250}} [string map {"\t" ""} $data]] {
		#	putdcc [hand2idx "joose"] $line
		#}
		#putdcc [hand2idx "joose"] "---------------------------------------"
		
		regexp {<div class="path">\s*?<strong>\s*?<a href="/browse">\s*?<img class="world" src="/img/world.gif" alt="Maailma" title="Maailma" width="18" height="20" />\s*?</a>\s*?<img src="/img/arrow2.gif" alt="&gt;&gt;" width="8" height="8" />\s*?<a href=".*?">(.*?)</a>\s*?<img src="/img/arrow2.gif" alt="&gt;&gt;" width="8" height="8" />\s*?<a href=".*?">(.*?)</a>\s*?(?:<img src="/img/arrow2.gif" alt="&gt;&gt;" width="8" height="8" />\s*?<a href=".*?">(.*?)</a>\s*?)?<img src="/img/arrow1.gif" alt="&gt;&gt;" width="4" height="8" /><a href=".*?">(.*?)</a>\s*?</strong>\s*?</div>} $data -> continent country state place
		set place [string trim $place]

		regexp {<div class="left">.*<span class=".*? txt-xxlarge"><strong>(.*?)</strong> (.*?)</span><br />.*} $data -> temp temp2
		set temp "$temp$temp2"
		#regexp {<div class="left">.*<img src="/img/symb-wind/.*?\.gif" alt="(.*?)" .*?/> <strong>(.*? m/s)</strong><br />.*} $data -> winddir windspeed
		regexp {<div class="left">.*Havaintoaika: <strong>(.*?)</strong><br />.*} $data -> time
		
		regexp {<div class="right txt-tight">\s*(?:([\w\s.,]*?)<br />\s*)?(?:Tuntuu kuin:\s*?<strong>(.*?&deg;)</strong><br />\s*)?(?:Ilmanpaine:\s*?<strong>(.*?)</strong><br />\s*)?(?:Kastepiste:\s*?<strong>(.*?&deg;)</strong><br />\s*)(?:Suht. kosteus:\s*?<strong>(.*?%)</strong><br />)?\s*(?:Näkyvyys:\s*?<strong>(.*?)</strong><br />\s*)?<br />\s*(?:(?:Aurinko nousee:\s*?<strong>(\d\d.\d\d)</strong><br />\s*)?(?:Aurinko laskee:\s*?<strong>(\d\d.\d\d)</strong><br />\s*)?(?:Päivän pituus:\s*?<strong>(.*?)</strong><br />\s*)?)</div>} $data -> weather feelslike pressure dewpoint humidity visibility sunrise sunset daylength
		
		#if { !($missing=="") && ($weather=="") } { set weather "Ei säähavaintoja" }
		
		#set times [saa::clearText $times]
		#set stationname [saa::clearText $stationname]
		#set weather [saa::clearText $weather]
		
		#set output [string map [list "%station%" $stationname "%weather%" $weather "%time%" $times] $saa::output]
		#set output [encoding convertto "utf-8" $output]
		
		#set output "Debug: Hakusana: \"$text\" -> Maa: \"$country\" Paikka: \"[string trim $place], $state\" Time: \"$time\" Station: \"$station\" Data: temp -> \"$temp $temp2\" winddir -> \"$winddir\" windspeed -> \"$windspeed\" weather -> \"$weather\" feelslike -> \"$feelslike\" pressure -> \"$pressure\" dewpoint -> \"$dewpoint\" humidity -> \"$humidity\" visibility -> \"$visibility\" sunrise -> \"$sunrise\" sunset -> \"$sunset\" daylength -> \"$daylength\""
		set output "Debug: Hakusana: \"$text\" -> Maa: \"$country\" Paikka: \"[string trim $place], $state\" Data: temp -> \"$temp $temp2\" Time: \"$time\""
		set output "Sää $place[expr {$state != "" ? ", $state" : ""}], $country ($time): $temp"
		set output [string map {"&deg;" " °C"} $output]
		
		#putdcc [hand2idx "joose"] "---------------------------------------"
		#foreach line [regexp -inline -all {.{0,250}} [string map {"\n" " "} $output]] {
		#	putdcc [hand2idx "joose"] $line
		#}
		#putdcc [hand2idx "joose"] "---------------------------------------"
		
		return [string map {"\n" " "} $output]
	}
	
	proc clearText { str } {
		set str [saa_foreca::urldecode $str]
		#set str [string map [list "&auml;" "ä" "&ouml;" "ö" "&Auml;" "Ä" "&Ouml;" "Ö" "&nbsp;" " " "&deg;" "°" "<br/>" " "] $str]
		regsub -all -- {\<[^\>]*\>|\t} $str "" str
		set str [string trim $str]
		return $str
	}
	
	proc url_decode { str } {
		set str [string map [list + { } "\\" "\\\\"] $str]
		regsub -all -- {%([A-Fa-f0-9][A-Fa-f0-9])} $str {\\u00\1} str
		return [subst -novar -nocommand $str]
	}
	
	proc url_encode { str } {	
		for {set i 0} {$i <= 256} {incr i} { 
			set c [format %c $i]
			if {![string match \[a-zA-Z0-9\] $c]} {
				set map($c) %[format %.2x $i]
			}
		}
		# These are handled specially
		array set map { " " + \n %0d%0a }

		regsub -all \[^a-zA-Z0-9\] $str {$map(&)} str
		# This quotes cases like $map([) or $map($) => $map(\[) ...
		regsub -all {[][{})\\]\)} $str {\\&} str
		return [subst -nocommand $str]
	}

	proc makequery {host url {post false} {cookie false} {ssl false} {port false} } {
		if {![string is false $post]} { 
			set haku "POST $url HTTP/1.1\n"
			append haku "Host: ${host}\n"
			if {![string is false $cookie]} {append haku "Cookie: ${cookie}\n"}
			append haku "User-Agent: Mozilla/5.0 ($::tcl_platform(os); U; $::tcl_platform(os) $::tcl_platform(machine); en) TCL/$::tcl_version\n"
			append haku "Content-Length: [string length $post]\n"
			append haku "Content-Type: application/x-www-form-urlencoded\n"
			append haku "Connection: close\n"
			append haku "\n"
			append haku $post
			append haku "\n"
		} else {
			set haku "GET $url HTTP/1.1\n"
			append haku "Host: ${host}\n"
			if {![string is false $cookie]} {append haku "Cookie: ${cookie}\n"}
			append haku "User-Agent: Mozilla/5.0 ($::tcl_platform(os); U; $::tcl_platform(os) $::tcl_platform(machine); en) TCL/$::tcl_version\n"
			append haku "Connection: close\n"
			append haku "\n"
		}
		
		set out ""		
		#set state_id [after 5000 set state timeout]
		if {$ssl == true} {
			package require tls
			if {$port == false} {
				set port 443
			}
			set sock [::tls::socket $host $port]
		} else {
			if {$port == false} {
				set port 80
			}
			set sock [socket $host $port]
		}
		
		#fconfigure $sock -blocking false
		puts $sock $haku
		flush $sock
		set out [read $sock]
		#vwait state
		close $sock
		#after cancel $state_id
		return $out
	}
}

putlog "${saa_foreca::scriptversion} loaded"
