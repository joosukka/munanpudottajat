bind time - "04 59 * * *" restartbot
proc restartbot { min hour day month year } {
	if { [clock format [clock seconds] -format %u] == 1 } {
		die "Reboot time!"
	}
}
